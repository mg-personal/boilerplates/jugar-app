import React from "react";

const Sport = (props) => {
  return (
    <div className="px-4">
      <p>{props.sportName}</p>
      <div>
        <div className="flex items-center w-full my-10">
          <div id="badge-icon" className="w-20 h-20 rounded-md flex justify-center items-center text-3xl font-black">
            F
          </div>
          <div className="flex items-center">
            <div className="pl-6">
              <p className="font-semibold">Boca Unidos</p>
              <p className="font-thin text-xs">Icons</p>
              <p className="font-standard text-xs text-gray-600">Rating</p>
            </div>
            <div className="pl-16">
              <p className="font-standard text-xs text-gray-600">2.1 km</p>
            </div>
          </div>
        </div>
      </div>
      <div className="h-px bg-gray-500 rounded-md w-full"></div>
    </div>
  );
};

export default Sport;
