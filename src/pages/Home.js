import React from "react";

import { Link } from "@reach/router";

const Home = () => {
  return (
    <div className="text-center flex flex-col items-center">
      <div className="hero-banner py-6">
        <p className="text-sm text-gray-300 italic font-sans mb-4">
          Estamos para ayudarte...
        </p>
        <p className="text-5xl text-yellow-500 font-sans italic font-bold tracking-tighter my-2">
          JUGA HOY!
        </p>
        <p className="text-sm text-gray-300 italic font-sans mt-4">
          La primer aplicacion que conecta jugadores con establecimientos
          deportivos.
        </p>
      </div>
      <div className="w-full py-6 px-2">
        <p className="text-3xl text-gray-900 font-medium tracking-tighter my-2">
          Reserva ya mismo, no pierdas tiempo.
        </p>
      </div>
      <div className="mt-20">
        <button className="px-12 py-2 bg-blue-800 text-gray-200 rounded-sm">
          {" "}
          <Link className="text-gray-300 text-lg" to="/explore">
            Explorar
          </Link>
        </button>
      </div>
    </div>
  );
};

export default Home;
