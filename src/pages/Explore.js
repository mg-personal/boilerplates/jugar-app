import React from "react";

import SportWidget from "../components/SportWidget";

const basquetIMG =
  "https://images.unsplash.com/photo-1519861531473-9200262188bf?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2251&q=80";
const futbolIMG =
  "https://images.unsplash.com/photo-1574629810360-7efbbe195018?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2656&q=80";
const tenisIMG =
  "https://images.unsplash.com/photo-1580153111806-5007b971dfe7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1934&q=80";
const golfIMG =
  "https://images.unsplash.com/photo-1576220258822-153014832245?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2264&q=80";
const natacionIMG =
  "https://images.unsplash.com/photo-1519315901367-f34ff9154487?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=900&q=60";

const Explore = () => {
  return (
    <div className="px-2">
      <p className="my-5 font-medium">Elegí el deporte!</p>
      <SportWidget sportName="Basquet" sportIMG={basquetIMG} />
      <SportWidget sportName="Futbol" sportIMG={futbolIMG} />
      <SportWidget sportName="Tenis" sportIMG={tenisIMG} />
      <SportWidget sportName="Golf" sportIMG={golfIMG} />
      <SportWidget sportName="Natacion" sportIMG={natacionIMG} />
    </div>
  );
};

export default Explore;
