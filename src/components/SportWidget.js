import React from "react";

import { Link } from "@reach/router";

const SportWidget = (props) => {
  return (
    <Link
      to={`/explore/${props.sportName}`}
      className="font-medium text-lg text-gray-300"
    >
      <div
        className="mt-10 mb-5 bg-cover bg-center flex items-center justify-center h-24 w-full rounded-lg"
        style={{
          // eslint-disable-next-line
          backgroundImage: "url(" + `${props.sportIMG}` + ")",
        }}
      >
        {props.sportName}
      </div>
    </Link>
  );
};

export default SportWidget;
