import React from "react";

import { Link } from "@reach/router";

const Navbar = () => {
  const showMenu = () => {
    console.log("hello Mauro");
    let nav = document.getElementById("navigation-menu");
    if (nav.classList.contains("hidden")) {
      nav.classList.remove("hidden");
    } else {
      nav.classList.add("hidden");
    }
  };

  return (
    <div>
      <div className="p-4 flex justify-between items-center" id="nav-bar">
        <Link to="/">
          <p className="text-3xl font-medium text-white">
            jug<span id="title-span">AR</span>
          </p>
        </Link>
        <button onClick={showMenu}>
          <i className="fas fa-bars fa-lg" id="menu-icon"></i>
        </button>
      </div>
      <div className="hidden rounded-b-lg absolute w-full" id="navigation-menu">
        <div className="h-32"></div>
        <div className="h-64 flex flex-col">
          <Link
            className="flex justify-center px-4 py-4 my-3 hover:bg-blue-900 transition-colors duration-300 text-gray-300 text-xl"
            to="/"
            onClick={showMenu}
          >
            <button>Inicio</button>
          </Link>

          <Link
            className="flex justify-center px-4 py-4 my-3 hover:bg-blue-900 transition-colors duration-300 text-gray-300 text-xl"
            to="/explore"
            onClick={showMenu}
          >
            <button>Explorar</button>
          </Link>
        </div>
        <div className="h-32"></div>
      </div>
    </div>
  );
};

export default Navbar;
