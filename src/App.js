import React from "react";
import "./index.css";

import { Router } from "@reach/router";

import Navbar from "./components/Navbar";

import Explore from "./pages/Explore";
import Home from "./pages/Home";
import Sport from "./pages/Sport";

function App() {
  return (
    <div>
      <Navbar />
      <div className="">
        {/* <Explore /> */}
        {/* <Home /> */}
        <Router>
          <Home path="/" />
          <Explore path="/explore" />
          <Sport path="/explore/:sportName" />
        </Router>
      </div>
    </div>
  );
}

export default App;

//TODO:
// - install @reach/router
// - set routes for 'home', 'explore', ':sportname' and 'default 404'
// - design the home landing page better
// - set roadmap or achievement list for the webapp
