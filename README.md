# jugAR

### [Check out the demo here](www.jugar.com.ar)

## About ⭐️

jugAR es una aplicación diseñada con el objetivo de servir de nexo entre el usuario que desea realizar algun deporte y el establecimiento deportivo
con sus medios de contacto.

## Technology 💡

Work in progress...

## Screenshots 📷

#### Home

<!-- ![home](https://i.imgur.com/1VLMsaz.png) -->

#### Explore

## License ⚠️

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
